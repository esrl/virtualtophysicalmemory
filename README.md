This program was written purely for enducational purpose, no real use case or application. The idea is to allocate an integer on the heap, take its virtual address and convert that into its corresponding physical address. Given this is working, it is possible to use that address to read the allocated value from /dev/mem*. Furthermore, the value in the C-program can be altered by changing the value directly in the /dev/mem device.

* In order to be able to access the physical memory through /dev/mem, the kernel must be compiled with CONFIG_STRICT_DEVMEM disabled, ortherwise you'll receive "permission denied" when trying to access the physical memory.

# Howto:
Compile using e.g. g++:
```
g++ main.c
```
and run using "./a.out".

In the beginning of the output, the program will print the virtual address.

In order to read the value from /dev/mem, use the following or simular:
```
sudo dd if=/dev/mem skip=$(( 16#1ab72fc20 )) ibs=1 count=4 bs=1  2>/dev/null | xxd
```
Let's break it down:
dd: is used to access and read from /dev/mem
skip: is the parameter to dd, that allows reading in the middle of the file, and not only from the beginning.
ibs:  size to do seeks
count: The number of bytes to read
bs: size to reads, 1 means one byte.
16#1ab72fc20: this simply converts the address in hex(output from the program) into  decimal as this is what dd uses to skip into the /dev/mem.

stderr is then redirected to /dev/null as information about how many bytes have been read is not very usefull. Stdout is then piped to xxd which showes the binary output from dd in hex.

Using the command shown below, the value of the address can be changes:
echo -n A | dd of=/dev/mem obs=1 seek=$(( 16#18ece9c20 ))

Note, A is in ascii and the program outputs the value of the address in hex...
