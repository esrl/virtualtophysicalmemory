#include <iostream>
#include <stdint.h> //for uint32_t
#include <fcntl.h> //for file opening
#include <unistd.h> //for NULL

/*
 * Some(most) of this code is stolen from https://www.raspberrypi.org/forums/viewtopic.php?t=181207
 * */

#define PAGE_SIZE 4096 //mmap maps pages of memory, so we must give it multiples of this size
uintptr_t virtToPhys(void* virt, int pagemapfd) {
    uintptr_t pgNum = (uintptr_t)(virt)/PAGE_SIZE;
    int byteOffsetFromPage = (uintptr_t)(virt)%PAGE_SIZE;
    uint64_t physPage;
    ///proc/self/pagemap is a uint64_t array where the index represents the virtual page number and the value at that index represents the physical page number.
    //So if virtual address is 0x1000000, read the value at *array* index 0x1000000/PAGE_SIZE and multiply that by PAGE_SIZE to get the physical address.
    //because files are bytestreams, one must explicitly multiply each byte index by 8 to treat it as a uint64_t array.
    int err = lseek(pagemapfd, pgNum*8, SEEK_SET);
    if (err != pgNum*8) {
        printf("WARNING: virtToPhys %p failed to seek (expected %lu got %i. errno: %i)\n", virt, pgNum*8, err, errno);
    }
    read(pagemapfd, &physPage, 8);
    if (!physPage & (1ull<<63)) { //bit 63 is set to 1 if the page is present in ram
        printf("WARNING: virtToPhys %p has no physical address\n", virt);
    }
    physPage = physPage & ~(0x1ffull << 55); //bits 55-63 are flags.
    uintptr_t mapped = (uintptr_t)(physPage*PAGE_SIZE + byteOffsetFromPage);
    return mapped;
}

int main(){
	int pagemapfd = open("/proc/self/pagemap", O_RDONLY);


	int *heapVirt = new int(0xBEEF);
	std::cout << "size of int: " << sizeof(int) << std::endl;

	uint64_t physHeap = virtToPhys(heapVirt,pagemapfd);
	std::cout << "phys heap: " << std::hex << physHeap << std::endl;
	std::cout << "Reads value from raw address using /dev/mem" << std::endl;

	int devmemfd = open("/dev/mem", O_RDONLY);
	uint64_t err = lseek(devmemfd, physHeap, SEEK_SET);
	if (err != physHeap) {
	    printf("WARNING: /dev/mem seek failed: got %lu errno: %i)\n", err, errno);
	}

	int val;
	read(devmemfd, &val,4);
	std::cout << "Int value from physical: " << val << std::endl;

	while(true){
		std::cout << "Value: " << *heapVirt << std::endl;
		sleep(1);
	}
	return 0;
}


